## 1.0.0 (2024-2-6)


### Features

* Added release script ([d5727f8](https://gitlab.com/kiloutyg/devopscicd/commit/d5727f8b3e20afffdeb5f99a0c5a8b472f366765))
* Added yaml script ([07c9d72](https://gitlab.com/kiloutyg/devopscicd/commit/07c9d72224d76b54e089be6a41bee9ad4f07cbfc))
* Added yaml script ([d7b73eb](https://gitlab.com/kiloutyg/devopscicd/commit/d7b73eb5d214a1133c22eb822e093f9c738fddfe))


### Bug Fixes

* issues encountered not letting pipelines work ([f8d609c](https://gitlab.com/kiloutyg/devopscicd/commit/f8d609c2c1a3837c865e5ef561f1b206441ca149))
